# Cloud Config Server

[![Build Status](http://jenkins.aba.land:8080/buildStatus/icon?job=Backend/Config Server/develop)](http://jenkins.aba.land:8080/job/Backend/job/Config%20Server/job/develop/)

Configuration files for aba projects based on [link](http://cloud.spring.io/spring-cloud-config/spring-cloud-config.html).

Spring Cloud Config provides server and client-side support for externalized configuration in a distributed system. With the Config Server you have a central place to manage external properties for applications across all environments. The concepts on both client and server map identically to the Spring Environment and PropertySource abstractions, so they fit very well with Spring applications, but can be used with any application running in any language. As an application moves through the deployment pipeline from dev to test and into production you can manage the configuration between those environments and be certain that applications have everything they need to run when they migrate. The default implementation of the server storage backend uses git so it easily supports labelled versions of configuration environments, as well as being accessible to a wide range of tooling for managing the content. It is easy to add alternative implementations and plug them in with Spring configuration.

For more informations see [spring-cloud-config](http://cloud.spring.io/spring-cloud-config/spring-cloud-config.html).

## Usage

Cloud Config Server exposes the following endpoints:
```
/{application}/{profile}[/{label}]
/{application}-{profile}.yml
/{label}/{application}-{profile}.yml
/{application}-{profile}.properties
/{label}/{application}-{profile}.properties
```

This configuration server is set to use a [repository](https://github.com/abaenglish/cloud-config-server-properties) with properties/yml files.

## Encryption and Decryption

We can encrypt and decrypt properties to avoid sensible data from being exposed.

We need to set an environment variable named ENCRYPT_KEY with the same value in all servers/containers. Jenkins is responsible to set this value in all deploys.

To set a encrypted property we can use any config server (dev and pro) to get the encrypted phrase:
```
curl http://cloud-config-server-pro.2grcvq3qpm.eu-west-1.elasticbeanstalk.com/encrypt -d data
```
Then we can set the result as:
```
zora.password: '{cypher}u4uibgsdibgsidbgisbfisbgisdbf'
```

spring-cloud-starter-config will be responsible for decrypt the value in spring boot applications.

See [more](http://cloud.spring.io/spring-cloud-config/spring-cloud-config.html#_encryption_and_decryption)
